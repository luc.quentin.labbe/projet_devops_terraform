provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "web" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
}

variable "availability_zones" {
  default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "public_subnet_cidrs" {
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "private_subnet_cidrs" {
  default = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
}

data "aws_ami" "web_ami" {
  owners      = ["770061062556"]
  most_recent = true
  filter {
    name   = "tag:Name"
    values = ["WebApp"]
  }
}

variable "instance_type" {
  default = "t2.micro"
}


# Create the public subnets
resource "aws_subnet" "private_subnets" {
  count = length(var.availability_zones)
  vpc_id = aws_vpc.web.id
  cidr_block = var.private_subnet_cidrs[count.index]
  availability_zone = var.availability_zones[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name = "WebPrivate_${count.index}"
  }
}

# Create the public subnets
resource "aws_subnet" "public_subnets" {
  count = length(var.availability_zones)
  vpc_id = aws_vpc.web.id
  cidr_block = var.public_subnet_cidrs[count.index]
  availability_zone = var.availability_zones[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name = "WebPublic_${count.index}"
  }
}

resource "aws_internet_gateway" "web_vpc_igw" {
  vpc_id = aws_vpc.web.id
  tags   = {
    Name = "Web"
  }
}

# Création de la table de routage privée
resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.web.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat.id
  }

  tags = {
    Name = "PrivateRouteTable"
  }
}

# Création de la table de routage public
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.web.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.web_vpc_igw.id
  }

  tags = {
    Name = "PublicRouteTable"
  }
}

resource "aws_route_table_association" "public" {
  count = length(aws_subnet.public_subnets)
  subnet_id = aws_subnet.public_subnets[count.index].id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "private" {
  count = length(aws_subnet.private_subnets)
  subnet_id = aws_subnet.private_subnets[count.index].id
  route_table_id = aws_route_table.private_route_table.id
}

resource "aws_security_group" "public" {
  vpc_id = aws_vpc.web.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "public-sg"
  }
}


resource "aws_security_group" "private" {
  vpc_id = aws_vpc.web.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.public.id]
  }

  tags = {
    Name = "private-sg"
  }
}

resource "aws_security_group" "load_balancer" {
  vpc_id = aws_vpc.web.id

  tags = {
    Name = "loadBalancer_sg"
  }
}

resource "aws_security_group" "auto_scaling" {
  vpc_id = aws_vpc.web.id

  tags = {
    Name = "autoScaling_sg"
  }
}

resource "aws_vpc_security_group_ingress_rule" "load_balancer_ingress_rule" {
  security_group_id = aws_security_group.load_balancer.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 443
  ip_protocol       = "tcp"
  to_port           = 443
}

resource "aws_vpc_security_group_egress_rule" "load_balancer_egress_rule" {
  security_group_id = aws_security_group.load_balancer.id
  referenced_security_group_id   = aws_security_group.auto_scaling.id
  from_port         = 443
  ip_protocol       = "tcp"
  to_port           = 443
}

resource "aws_vpc_security_group_ingress_rule" "auto_scaling_rule_ingress" {
  security_group_id = aws_security_group.auto_scaling.id
  referenced_security_group_id   = aws_security_group.load_balancer.id
  from_port         = 443
  ip_protocol       = "tcp"
  to_port           = 443
}



resource "aws_eip" "nat" {
  domain = "vpc"
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public_subnets[0].id

  tags = {
    Name = "nat-gateway"
  }
}

# ASG Launch Configuration
resource "aws_launch_configuration" "web" {
  image_id = data.aws_ami.web_ami.id
  instance_type = var.instance_type
  key_name = "vockey"
  iam_instance_profile = "LabInstanceProfile"
  security_groups = [aws_security_group.auto_scaling.id]
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_launch_template" "web_template" {
  name = "web_template"
  iam_instance_profile {
    name = "LabInstanceProfile"
  }
  image_id        = data.aws_ami.web_ami.id
  instance_type   = var.instance_type
  key_name        = "vockey"
}

# Load Balancer
resource "aws_lb" "web" {
  name = "web"
  internal = false
  load_balancer_type = "application"
  subnets = [for subnet in aws_subnet.public_subnets : subnet.id]
  enable_deletion_protection = false
  security_groups = [aws_security_group.load_balancer.id]
}

resource "aws_lb_target_group" "web" {
  name = "web"
  port = 443
  protocol = "HTTP"
  vpc_id = aws_vpc.web.id
  health_check {
    enabled = true
    healthy_threshold = 3
    interval = 15
    path = "/"
    port = 443
    timeout = 5
    unhealthy_threshold = 3
  }
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.web.arn
  port = "443"
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.web.arn
  }
}

resource "aws_autoscaling_group" "web" {
  target_group_arns    = [aws_lb_target_group.web.arn]
  name                 = "Web_autoscaling_group"
  desired_capacity     = 2
  max_size             = 5
  min_size             = 1
  vpc_zone_identifier  = [for subnet in aws_subnet.public_subnets : subnet.id]
  launch_configuration = aws_launch_configuration.web.name
  health_check_type        = "EC2"
  health_check_grace_period = 30
  force_delete             = true
}

# DNS name of the ELB (wait for a few minutes before it becoming accessible on first deployment)
output "lb_dns_name" {
  description = "The DNS name of the ELB"
  value = aws_lb.web.dns_name
}